Official System Distribution repository for the ZX Spectrum Next computer

Current versions  
================  

|Title |Version|  
|---	|--:	|  
|*System/Next*| **24.11** |  
|*NextZXOS*|**2.09** |  
|*Core (issue 2)*|**3.02.00** |  
|*Core (issue 4)*|**3.02.01** |  
|*Firmware*|**1.44.db** |  
  
  
Contributor Requests
====================
If you wish to contribute to the distribution, a request via gitlab is not
the only step you need to take; you need to get in touch with one of the other
contributors and describe the nature of your planned contribution.  
This is a necessary step as this is the **official** distribution and legality / licenses
**MUST** be strictly observed.
  
  
Acknowledgments / Copyrights  
============================
All softwares contained herewith are © by their respective authors and provided under license.  
**_Sinclair_** and **_ZX Spectrum_** are © **Amstrad / Sky plc** and are used with permission under license.  
**_esxDOS_** is © **Miguel Guerreiro / Papaya Dezign**.  
**_divMMC_** is © **Mario Prato**.  
The **_ZX 80_** and **_ZX 81_** *EMULATORS* are © **Paul Farrow**.  
**_Gosh Wonderful_** and **_Looking Glass_** are © **Geoff Wearmouth**.  
_**+3e**_, **_NextZXOS_** and **_NextBASIC_** are © **Garry Lancaster**.  
**_TBBlue_** is © **Victor Trucco**, **Fabio Belavenuto**.  
**_Spectrum Next_** and **_System/Next_** are © **SpecNext Ltd**.  
**Firmware 1.44.db** contains contributions © 2024 by **David Banks**.
**Ql Core** is © 2024 **LionT, Victor Trucco, Till Harbaum**. Additional QL Hardware is © 2024 **Peter Graf, Marcel Kilgus, LionT, gØd** 
